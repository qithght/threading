package threading;

public class Villains extends Human {
	private int cruelty = 0;
	
	public int getCruelty(){
		return cruelty;
	}
	public void setCruelty(int cruelty){
		this.cruelty = cruelty;
	}
	public void attack(){
		super.attack();
		GameEngine.getInstance().attackAnyHero(getDamage());
	}
	
}
