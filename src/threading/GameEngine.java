package threading;
import java.util.*;



public class GameEngine{

	//Making GameEngine into a singleton class
	private static GameEngine instance = null;
	protected GameEngine(){
		
	}
	public static GameEngine getInstance(){
		if(instance == null){
			instance = new GameEngine();
		}
		return instance;
	}
	//End Singleton
	
	//declaring some randomly generated number
	private final static Random randNum = new Random();
	LinkedList<Human> Heroes = new LinkedList<Human>();
	LinkedList<Human> Villains = new LinkedList<Human>();
	public void addHero(){
		int rng1 = randNum.nextInt(2);
		System.out.println("Adding Hero of type " + rng1);
		if(rng1==0){
		Heroes.add(new RedKnight());
		}else if(rng1==1){
		Heroes.add(new PhantomArcher());
		}
	}
	
	public void addVillain(){
		int rng1 = randNum.nextInt(2);
		System.out.println("Adding a villain of type " + rng1);
		if(rng1==0){
		Villains.add(new ShadeKnight());
		
		}else if(rng1==1){
		Villains.add(new AcidMage());
		}
	}
	
	public  synchronized void attackAnyHero(int damage){
		
		int target = randNum.nextInt(Heroes.size());
		Heroes.get(target).takeDamage(damage);
		System.out.println(Heroes.get(target).getName() + " had taken " + damage+" points of damage.");
		if(Heroes.get(target).getHealth() <=0 ){
			System.out.println(Heroes.get(target).getName() + " has been slain!");
			Heroes.remove(target);
			gameStatus();
		}
	}
	
	public synchronized void attackAnyVillain(int damage){
		
		int target = randNum.nextInt(Villains.size());
		Villains.get(target).takeDamage(damage);
		System.out.println(Villains.get(target).getName() + " had taken " + damage+" points of damage.");

		if(Villains.get(target).getHealth() <=0 ){
			System.out.println(Villains.get(target).getName() + " has been slain!");
			Villains.remove(target);
			gameStatus();
		}
		
	}
	
	public void gameStatus(){
		if(Heroes.peekFirst()== null){
			System.out.println("The Villains have prevailed and defeated the heroes in mortal combat!");
//				for(int x = Villains.size(); x > 0; x--){
//					Villains.get(x-1).setHealth(0);
//				}
//		
			
			
			for(Human human: Villains){
				human.setHealth(0);
			}
		}else if(Villains.peekFirst()== null){
			System.out.println("The Heroes have defeated the Villains in mortal combat!");
//			for(int x = Heroes.size(); x > 0; x--){
//				Heroes.get(x-1).setHealth(0);
//			}
			for(Human human: Heroes){
				human.setHealth(0);
			}
		}
		
	}
	
	public void gameStart(){
		
		for(int x = 0; x < Heroes.size(); x++){
			(new Thread(this.Heroes.get(x))).start();
			
		}
		
		for(int x = 0; x < Villains.size(); x++){
			(new Thread(this.Villains.get(x))).start();
		}
		
	}
	
}
