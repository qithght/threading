package threading;

public class Heroes extends Human{
		private int luck = 6;
		
		public int getLuck(){
			return luck;
		}
		public void setLuck(int luck){
			this.luck = luck; 
		}
		
		public void attack(){
			super.attack();
			GameEngine.getInstance().attackAnyVillain(getDamage());
		}
		
}
