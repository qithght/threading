package threading;

public abstract class Human implements Runnable{
	private String name = "no name";
	private int health = 100;
	private int mana = 100;
	private int damage = 10;
	private int delay = 2000;


	
	public void run(){
		//doing something here
		System.out.println(name + " has entered the field of battle!");
	
		
		try{
			Thread.sleep(6000);
		}
		catch(InterruptedException exception){
			System.out.println(name + " terminated early due to interruption");
		}
		
		
		while(getHealth() > 0){
		try{
			
			Thread.sleep(getDelay());
			System.out.println(getName() + " has " + getHealth() + " hit points");
		}catch(InterruptedException exception){
			System.out.println(name + " terminated early due to interruption");
		}
		
	    if(getHealth() <= 0){
	    	break;
	    }
	    	attack();
	    System.out.println("The attacker's name is: " + getName()+ " who's hp is: " + getHealth());
		}
	}
	
	

	public void attack(){
		
	}
		
		
	



	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name = name;
	}
	public int getHealth(){
		return health;
	}
	public void setHealth(int health){
		this.health = health;
	}
	public void setMana(int mana){
		this.mana = mana;
	}
	public int getMana(){
		return mana;
	}
	public int getDamage(){
		return damage;
	}
	public void setDamage(int damage){
		this.damage = damage;
	}
	public int getDelay(){
		return delay;
	}
	public void setDelay(int delay){
		this.delay = delay;
	}

	public void takeDamage(int damage){
		health = health - damage;
	}
	
}
