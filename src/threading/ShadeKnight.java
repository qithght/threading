package threading;

public class ShadeKnight extends Villains {
	private String name = "Shade Knight";
	private int health = 250;
	private int mana = 200;
	private int damage = 8;
	private int cruelty = 0;

	
	
	public ShadeKnight(){
		this.setName(name);
		this.setHealth(health);
		this.setMana(mana);
		this.setDamage(damage);
		this.setCruelty(cruelty);
		
	}
	
	public int DragonsFang(){
		System.out.println("Shade Knight uses his special attack Dragon's Fang!");
		return damage * 3;
	}
	
}
