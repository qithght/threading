package threading;
//testing threads
public class Main {

	public static void main(String[] args){
/*	
		Thread hero1 = new Thread (new PhantomArcher());
		Thread villain1 = new Thread (new ShadeKnight());
		Thread hero2 = new Thread (new RedKnight());
		Thread villain2 = new Thread(new AcidMage());
		
		hero1.start();
		villain1.start();
		hero2.start();
		villain2.start();
		
*/		
		GameEngine gameEngine = new GameEngine();
		gameEngine.getInstance().addVillain();
		gameEngine.getInstance().addVillain();
		gameEngine.getInstance().addHero();
		gameEngine.getInstance().addHero();
		gameEngine.getInstance().gameStart();
	}
	
}
