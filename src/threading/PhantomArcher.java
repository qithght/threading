package threading;

public class PhantomArcher extends Heroes {

	private String name = "Phantom Archer";
	private int health = 200;
	private int mana = 200;
	private int damage = 8;
	private int luck = 9;
	
	
	public PhantomArcher(){
		this.setName(name);
		this.setHealth(health);
		this.setMana(mana);
		this.setDamage(damage);
		this.setLuck(luck);
		
	}
	
	
	
}
